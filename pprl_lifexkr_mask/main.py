import csv
import json
import logging
from enum import Enum
from json import JSONDecodeError
from pathlib import Path
from typing import Annotated, Any

import requests
import typer
from requests import RequestException

from rich.logging import RichHandler

logging.basicConfig(level="NOTSET", format="%(message)s", datefmt="[%X]", handlers=[RichHandler()])
log = logging.getLogger("main")

# requests logger to should only print warnings
logging.getLogger("requests").setLevel(logging.WARNING)
logging.getLogger("urllib3").setLevel(logging.WARNING)

JsonObject = dict[str, Any]


class CsvDelimiter(str, Enum):
    comma = ","
    semicolon = ";"
    tab = "tab"


def post_json(url: str, obj: JsonObject) -> JsonObject:
    try:
        r = requests.post(url, json=obj)
    except RequestException as e:
        log.error("Unexpected error while sending request", exc_info=e)
        raise typer.Exit(1)

    if r.status_code != 200:
        log.error("Unexpected status code: %d", r.status_code)
        log.error("Response body: %s", r.text)
        raise typer.Exit(1)

    try:
        return r.json()
    except requests.JSONDecodeError as e:
        log.error("Response didn't contain valid JSON", exc_info=e)
        raise typer.Exit(1)


def main(
        input_file: Annotated[
            Path, typer.Argument(help="Path to input file.", exists=True, dir_okay=False, file_okay=True,
                                 readable=True)] = Path("./data/data.csv"),
        output_file: Annotated[Path, typer.Argument(help="Path to output file.", writable=True, dir_okay=False)] = Path(
            "./data/output.csv"),
        attribute_dir_path: Annotated[
            Path, typer.Option(help="Path to attribute directory.", exists=True, dir_okay=True, file_okay=False,
                               readable=True)] = Path("./attributes"),
        mapping_file_path: Annotated[
            Path, typer.Option(help="Path to attribute mapping file.", exists=True, dir_okay=False, file_okay=True,
                               readable=True)] = Path("./data/mapping.json"),
        delimiter: Annotated[
            CsvDelimiter, typer.Option("--delimiter", "-d",
                                       help="Column delimiter in input and output file.")] = CsvDelimiter.comma.value,
        batch_size: Annotated[int, typer.Option("--batch", "-b", help="Amount of records to process at once.")] = 100,
        transform_url: Annotated[
            str, typer.Option(help="URL to transform service endpoint.")] = "http://localhost:8080/transform",
        mask_url: Annotated[str, typer.Option(help="URL to mask service endpoint.")] = "http://localhost:8080/mask",
        hash_key: Annotated[str, typer.Option(help="Key for Bloom filter based hashing.")] = "s3cr3t",
        token_size: Annotated[int, typer.Option(help="Text token size.")] = 2,
        hash_values: Annotated[int, typer.Option(help="Amount of base hash values per attribute.")] = 5
):
    if delimiter == CsvDelimiter.tab:
        delimiter = "\t"

    attribute_config_dict: [str, JsonObject] = {}

    log.info("Reading attribute configuration...")

    for attribute_file in attribute_dir_path.glob("*.json"):
        attribute_name = attribute_file.stem

        try:
            with attribute_file.open(encoding="utf-8", mode="r") as f:
                d = json.load(f)

                if "mask" not in d:
                    log.error("Attribute config file %s is missing mandatory `mask` key", attribute_file)
                    raise typer.Exit(1)

                attribute_config_dict[attribute_name] = d
        except JSONDecodeError as e:
            log.error("Attribute config file %s doesn't contain valid JSON", attribute_file, exc_info=e)
            raise typer.Exit(1)

    log.info("Loaded attributes: %s", ", ".join(list(attribute_config_dict.keys())))

    log.info("Reading attribute mapping...")

    try:
        with mapping_file_path.open(mode="r", encoding="utf-8") as f:
            attribute_mapping_dict = json.load(f)
    except JSONDecodeError as e:
        log.error("Attribute mapping file %s doesn't contain valid JSON", mapping_file_path, exc_info=e)
        raise typer.Exit(1)

    attribute_mapping_id_col_key = "idColumn"
    attribute_mapping_map_key = "attributeMapping"

    if attribute_mapping_id_col_key not in attribute_mapping_dict:
        log.error(f"Attribute mapping file is missing mandatory `{attribute_mapping_id_col_key}` key")
        raise typer.Exit(1)

    if attribute_mapping_map_key not in attribute_mapping_dict:
        log.error(f"Attribute mapping file is missing mandatory `{attribute_mapping_map_key}` key")
        raise typer.Exit(1)

    # id will be coerced to a string
    id_column_name = str(attribute_mapping_dict[attribute_mapping_id_col_key])

    if type(attribute_mapping_dict[attribute_mapping_map_key]) != dict:
        log.error(f"`{attribute_mapping_map_key}` in attribute mapping file must be an object")
        raise typer.Exit(1)

    raw_input_column_to_attribute_name_dict: JsonObject = attribute_mapping_dict[attribute_mapping_map_key]

    # attribute names will be coerced to strings
    input_column_to_attribute_name_dict = {
        k: str(raw_input_column_to_attribute_name_dict[k]) for k in raw_input_column_to_attribute_name_dict.keys()
    }

    log.info("Name of ID column: %s", id_column_name)
    log.info("Column to attribute mapping: %s", ", ".join([
        f"{k} => {v}" for k, v in input_column_to_attribute_name_dict.items()
    ]))

    # make sure that all attributes have a config
    missing_attribute_configs = [
        v for v in input_column_to_attribute_name_dict.values() if v not in attribute_config_dict
    ]

    # throw an error if that's not the case
    if len(missing_attribute_configs) != 0:
        log.error("There are missing configs for the following attributes: %s", ", ".join(missing_attribute_configs))
        raise typer.Exit(1)

    log.info("Processing input file %s, %d records at a time...", input_file, batch_size)

    with output_file.open(newline="", mode="w", encoding="utf-8") as output_handle:
        writer = csv.DictWriter(output_handle, delimiter=delimiter, fieldnames=[id_column_name, "value"])
        writer.writeheader()

        with input_file.open(newline="", mode="r", encoding="utf-8") as input_handle:
            reader = csv.DictReader(input_handle, delimiter=delimiter)

            # check that columns in mapping are also present in the csv file
            missing_columns = [c for c in input_column_to_attribute_name_dict.keys() if c not in reader.fieldnames]

            # throw an error if that's not the case
            if len(missing_columns) != 0:
                log.error("There are columns in the mapping file that are not present in the input file: %s",
                          ", ".join(missing_columns))
                raise typer.Exit(1)

            # collect all attribute configs for the relevant columns. we pull the columns that we want to use
            # from the mapping config since it's easier to edit than removing columns from the input file.
            selected_columns = list(input_column_to_attribute_name_dict.keys())

            # pull configs from attributes
            mask_configs = [
                # first element is attribute name, second is mask configuration
                (v, attribute_config_dict[v]["mask"])
                for v in input_column_to_attribute_name_dict.values()
            ]

            transform_configs = [
                # first element is attribute name, second is transformer configuration
                (v, attribute_config_dict[v]["transform"])
                for v in input_column_to_attribute_name_dict.values()
                if "transform" in attribute_config_dict[v]
            ]

            should_exit = False
            total_entity_count = 0

            while not should_exit:
                next_entities: list[JsonObject] = []

                try:
                    for _ in range(batch_size):
                        input_row: JsonObject = next(reader)

                        next_entity = {"id": input_row[id_column_name]}
                        next_entity.update({
                            # map the column to the correct attribute
                            input_column_to_attribute_name_dict[c]: str(v) for c, v in input_row.items()
                            # ignore the id column because we handled that already and only keep columns that are
                            # in the column to attribute mapping
                            if c != id_column_name and c in selected_columns
                        })

                        next_entities.append(next_entity)
                except StopIteration:
                    # this will be thrown when there are no more rows, so we should exit this loop at the next chance
                    should_exit = True

                # this case can happen when the input file contains a multiple of the batch size
                # in this case we can exit preemptively
                if len(next_entities) == 0:
                    break

                # run preprocessing step
                r = post_json(transform_url, {
                    "entities": next_entities,
                    "globalTransformers": [
                        {
                            "name": "norm",
                            "order": "before"
                        }
                    ],
                    "attributeTransformers": [
                        {
                            "attributeName": t[0],
                            "transformers": t[1]
                        } for t in transform_configs
                    ]
                })

                # check that the response contains the `entities` key
                if "entities" not in r:
                    log.error("Transformer output is malformed: %s", extra=r)
                    raise typer.Exit(1)

                next_entities = r["entities"]

                # run masking step
                r = post_json(mask_url, {
                    "masking": {
                        "tokenSize": token_size,
                        "hashValues": hash_values,
                        "hashFunction": "HMAC-SHA256",
                        "key": hash_key,
                        "seed": 123,
                        "hashStrategy": "randomHash",
                        "filterType": "CLKRBF",
                        "hardeners": [
                            # {
                            #     "name": "balance"
                            # },
                            {
                                "name": "permute",
                                "seed": 456
                            }
                        ]
                    },
                    "entities": next_entities,
                    "attributes": [
                        {
                            "name": c[0],
                            **c[1]
                        } for c in mask_configs
                    ]
                })

                # check that the response contains the `entities` key
                if "entities" not in r:
                    log.error("Mask output is malformed: %s", extra=r)
                    raise typer.Exit(1)

                next_entities = r["entities"]

                writer.writerows(
                    {
                        id_column_name: e["id"],
                        "value": e["value"]
                    } for e in next_entities
                )

                total_entity_count += len(next_entities)
                log.info("Wrote %d rows, %d in total", len(next_entities), total_entity_count)

    log.info("I think we're done here!")


if __name__ == "__main__":
    typer.run(main)
