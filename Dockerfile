FROM python:3.11.3-alpine

WORKDIR /app
COPY . .

RUN pip install poetry -q && poetry install -q

ENTRYPOINT ["poetry", "run", "pprl_transform_mask"]

CMD ["-h"]