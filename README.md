# LIFExKR entity transformation and masking

This project contains scripts and service definitions to perform entity transformation and masking for privacy-preserving record linkage.

## How to use

You need to [install Docker and Docker Compose](https://docs.docker.com/engine/install/).
You will need a CSV file with data that you want to mask.
This CSV file may look as follows.

| **id** | **first_name** | **last_name** | **gender** | **...** |
|--------|----------------|---------------|------------|---------|
| 001    | Maximilian     | Jugl          | M          | ...     |
| 002    | Lars           | Hempel        | M          | ...     |
| 003    | Ulrike         | Klotz         | W          | ...     |
| ...    | ...            | ...           | ...        | ...     |

Copy it to the [data directory](./data) and rename it to `data.csv`.
Next, you need to map the CSV columns to attribute types.
Open the example [mapping file](./data/mapping.json) in a text editor of your choice.

```json
{
  "idColumn": "id",
  "attributeMapping": {
    "first_name": "firstname",
    "last_name": "lastname",
    "gender": "gender"
  }
}
```

Make sure that `idColumn` contains the name of the ID column in your CSV file.
Next, edit `attributeMapping`.
For every column in your CSV file, add an entry as `"csv_column_name": "attribute_name"`.
You can find the corresponding `attribute_name` by looking up the files in the [attributes directory](./attributes/).
Use the file name **without the file extension**.

Finally, edit the [Docker Compose file](./docker-compose.yml).
Go to `command` and scroll to the end.
Run `id -a` in a console to determine your `uid` and `gid`.
Edit the `command` and replace the `--chown` argument with your user's `uid` and `gid`.

Run `docker compose up -d`.
A container for the script should be built and executed.
The execution of the script shouldn't take too long.
You can check whether the script container has completed by running `docker compose ps`.
The script container must complete with the exit code 0.
By the end, you will be able to find an output file in the [data directory](./data/).

### Notes for the LIFExKR use case

There are seven attributes that have been selected for the LIFExKR use case
Wherever possible, the format of the attribute values is inferred from the corresponding definition in the [Basisdatensatz](https://basisdatensatz.de/basisdatensatz).
This affects the following attributes:

- 3.3 Last name
- 3.6 First name
- 3.9 Gender
- 3.10 Date of birth
- 3.11 Street of residence
- 3.12 Street/house number of residence
- 3.14 Post code of residence
- 3.15 City of residence

There are a total of seven different attribute combinations that are tested, each with and without phonetic encoding.
This makes for a total of 14 hashes to generate for each record.
These attribute combinations can be found in the [attribute config directory](attributes).

To generate the hashes, first edit the [attribute mapping file](./data/mapping.json).
It should look something like this, where the keys in the `attributeMapping` object refer to column names and the values refer to the attribute names.
Do not change the values, only adjust the object keys and the `idColumn` property to link the generated hashes to an existing identifier.
The resulting file should look like this.

```json
{
  "idColumn": "id",
  "attributeMapping": {
    "birth_date": "birthdate",
    "building_no": "buildingno",
    "city": "city",
    "first_name": "firstname",
    "gender": "gender",
    "last_name": "lastname",
    "street": "street",
    "plz": "zip"
  }
}
```

Open the [Docker Compose file](docker-compose.yml) and scroll to the end until you find the `command` section.
Adjust the `--chown` parameter as described in the previous section.
Append an extra parameter `--attribute-dir-path ./attributes/ncvr-e01`.
For large input files, add `-b 1000` to process the input file in chunks of 1000 records.
By the end, the `script` section of the Docker Compose file should look like this.

```yml
script:
build:
  dockerfile: Dockerfile
  context: .
command: ./data/mapping.json ./data/data.csv ./data/output.csv --mask-url http://masker:8080/ --transform-url http://transformer:8080/ --chown 1000:1000 --attribute-dir-path ./attributes/ncvr-e01
volumes:
  - ./data:/app/data
depends_on:
  - masker
  - transformer
```

Now run `docker compose up -d` as described above.

To generate hashes for all attribute combinations, you will need to change the `--attribute-dir-path` parameter between runs.

## License

MIT.